import { createApp } from "vue";
import { createI18n } from "vue-i18n";
import en from "./locale/en.json";
import th from "./locale/th.json";
import de from "./locale/de.json";
import App from "./App.vue";

const i18n = createI18n({
  legacy: false,
  locale: "th",
  fallbackLocale: "en",
  messages: {
    en: en,
    th: th,
    de: de
  }
});

createApp(App)
  .use(i18n)
  .mount("#app");
