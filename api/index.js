const express = require("express");
const cors = require("cors");

const app = express();
app.use(express.json());
app.use(cors());

const text = {
  en:
    "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Varius sit amet mattis vulputate enim. Arcu cursus vitae congue mauris rhoncus aenean vel elit scelerisque.",
  de:
    "Deutsches Ipsum Dolor deserunt dissentias Hörspiele et. Tollit argumentum ius an. zu spät lobortis elaboraret per ne, nam Bildung probatus pertinax, impetus eripuit aliquando Die Toten Hosen sea. Diam scripserit no vis, zu spät meis suscipit ea. ",
  th:
    "ยาวี อัลบั้ม คอลเล็กชั่นแชมเปี้ยนชีส ปิโตรเคมีแอคทีฟ เคอร์ฟิวซาดิสม์สตรอเบอร์รีอริยสงฆ์ ซัพพลายเออร์สหรัฐสโตนติว โอเวอร์ดีพาร์ตเมนต์คอร์ปแฮมเบอร์เกอร์ไรเฟิล รีวิวลีเมอร์แอโรบิคโชว์รูม นอร์ทจีดีพีบริกรแซ็กโซโฟนหยวน อาว์ แชเชือนวโรกาสโดนัทยะเยือก ตาปรือ พันธุวิศวกรรม ธรรมาแซ็ก เซ็นทรัล ป๋อหลอคอลัมน์วอเตอร์ไอเดีย"
};

app.get("/text", (req, res) => {
  const locale = req.header("Accept-Language").substr(0, 2);
  res.send({ text: text[locale] });
});

app.post("/like", (req, res) => {
  res.send({ message: "thank_you" });
});

app.listen(8008);
